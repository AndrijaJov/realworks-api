<?php

declare(strict_types=1);

namespace Drupal\realworks_api\Plugin\QueueWorker;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\realworks_api\Service\RealworksSync;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The Realworks sync queue worker class.
 *
 * @QueueWorker(
 *   id = "realworks_sync",
 *   title = @Translation("Queue worker for realworks sync."),
 *   cron = {"time" = 360}
 * )
 */
class RealworksSyncQueueWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {
  /**
   * The Realworks sync service.
   *
   * @var \Drupal\realworks_api\Service\RealworksSync
   */
  protected $realworksSync;

  /**
   * Constructor.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param array $plugin_definition
   *   The plugin definition.
   * @param \Drupal\realworks_api\Service\RealworksSync $realworksSync
   *   The Realworks sync service.
   */
  public function __construct(array $configuration,
                              string $plugin_id,
                              array $plugin_definition,
                              RealworksSync $realworksSync) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->realworksSync = $realworksSync;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('realworks_api.sync'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    foreach ($data as $item) {
      $this->realworksSync->processItem($item);
    }
  }

}
