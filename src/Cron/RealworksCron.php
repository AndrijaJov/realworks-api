<?php

namespace Drupal\realworks_api\Cron;

use Drupal\Core\Queue\QueueFactory;
use Drupal\realworks_api\Service\RealworksSync;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class to process the Cron job.
 */
class RealworksCron {
  /**
   * The Realworks sync service.
   *
   * @var \Drupal\realworks_api\Service\RealworksSync
   */
  protected $realworksSync;
  /**
   * The Drupal queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * Constructor.
   *
   * @param \Drupal\realworks_api\Service\RealworksSync $realworksSync
   *   The Realworks sync service.
   * @param \Drupal\Core\Queue\QueueFactory $queueFactory
   *   The Drupal queue factory.
   */
  public function __construct(RealworksSync $realworksSync, QueueFactory $queueFactory) {
    $this->realworksSync = $realworksSync;
    $this->queueFactory = $queueFactory;
  }

  /**
   * Dependency injection method.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The dependency injection container.
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('realworks_api.sync'), $container->get('queue'));
  }

  /**
   * Calls Realworks Sync service.
   *
   * @throws \Exception
   */
  public function syncProperties() : void {
    $rw_sync = \Drupal::service('realworks_api.sync');

    $items = $rw_sync->getItems();
    $batches = array_chunk($items, 10);

    $queue = $this->queueFactory->get('realworks_sync', TRUE);
    $queue->createQueue();

    foreach ($batches as $batch) {
      $queue->createItem($batch);
    }
  }

}
