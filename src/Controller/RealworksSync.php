<?php

namespace Drupal\realworks_api\Controller;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Queue\QueueFactory;

/**
 * Provides an API for syncing with Realworks.
 */
class RealworksSync extends ControllerBase {
  /**
   * The Drupal queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Queue\QueueFactory $queueFactory
   *   The Drupal queue factory.
   */
  public function __construct(QueueFactory $queueFactory) {
    $this->queueFactory = $queueFactory;
  }

  /**
   * Dependency injection create method.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The dependency injection container.
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('queue'));
  }

  /**
   * Pulls data from Realworks and creates/updates Drupal content type.
   */
  public function pull() {
    $rw_sync = \Drupal::service('realworks_api.sync');

    $items = $rw_sync->getItems();
    $batches = array_chunk($items, 10);

    $queue = $this->queueFactory->get('realworks_sync', TRUE);
    $queue->createQueue();

    foreach ($batches as $batch) {
      $queue->createItem($batch);
    }

    return ['#markup' => $this->t('<h4>Pull successful</h4><br><p>Data has been pulled from Realworks API, it will now be processed in the background.</p>')];
  }

}
