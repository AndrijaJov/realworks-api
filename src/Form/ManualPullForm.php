<?php

namespace Drupal\realworks_api\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Manual pull form for the Realworks API.
 */
class ManualPullForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'realworks_api_pull';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['realworks_api.pull'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'Pull data from Realworks',
      '#button_type' => 'primary',
      '#prefix' => '<p>Initiate a manual pull of data from the Realworks API.</p>',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirectUrl(Url::fromRoute('realworks_api.pull'));
  }

}
