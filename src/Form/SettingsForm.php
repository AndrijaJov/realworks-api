<?php

namespace Drupal\realworks_api\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Settings form for the Realworks API module.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'realworks_api_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['realworks_api.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('realworks_api.settings');

    $form['wonen'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Wonen API Information'),
      '#default_value' => $config->get('wonen'),
      '#required' => FALSE,
      '#description' => $this->t('One endpoint per row in the format: url|key'),
    ];
    $form['bedrijven'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Bedrijven API Information'),
      '#default_value' => $config->get('bedrijven'),
      '#required' => FALSE,
      '#description' => $this->t('One endpoint per row in the format: url|key'),
    ];
    $form['agrarisch'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Agrarisch API Information'),
      '#default_value' => $config->get('agrarisch'),
      '#required' => FALSE,
      '#description' => $this->t('One endpoint per row'),
    ];

    $form['geocoding_api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Geocoding API Key'),
      '#default_value' => $config->get('geocoding_api_key'),
      '#required' => FALSE,
      '#description' => $this->t('API key for Google Geocoding API'),
      '#prefix' => '<hr>',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('realworks_api.settings')
      ->set('wonen', $form_state->getValue('wonen'))
      ->set('bedrijven', $form_state->getValue('bedrijven'))
      ->set('agrarisch', $form_state->getValue('agrarisch'))
      ->set('geocoding_api_key', $form_state->getValue('geocoding_api_key'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
