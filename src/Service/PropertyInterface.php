<?php

namespace Drupal\realworks_api\Service;

/**
 * Interface for parsing a property and creating its node.
 */
interface PropertyInterface {

  /**
   * Prepares directories used for storing files pulled from Realworks.
   */
  public function prepareMediaDirectories();

  /**
   * Sets the pulled API data and the existing node.
   */
  public function setData($apiData, $existing);

  /**
   * Parses all fields from the pulled API data.
   */
  public function parseAll();

  /**
   * Creates a Drupal node using the parsed fields.
   */
  public function createNode();

}
