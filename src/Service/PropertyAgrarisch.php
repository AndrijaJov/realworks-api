<?php

namespace Drupal\realworks_api\Service;

use Drupal\Core\File\FileSystemInterface;
use Drupal\file\Entity\File;
use GuzzleHttp\Exception\ClientException;

/**
 * Class for parsing API data from an Agrarisch endpoint.
 */
class PropertyAgrarisch extends PropertyBase {

  /**
   * {@inheritDoc}
   */
  public function parseSource() {
    $this->source = 'agrarian';
  }

  /**
   * {@inheritDoc}
   */
  public function parseId() {
    if (isset($this->apiData['ObjectSystemID'])) {
      $this->id = 'rw_agrarisch:' . $this->apiData['ObjectSystemID'];
    }
    else {
      $this->id = NULL;
    }
  }

  /**
   * {@inheritDoc}
   */
  public function parsePriceType() {
    $this->priceType = NULL;
    if (isset($this->apiData['ObjectDetails']['Koop']['Koopprijs'])) {
      $this->priceType = 'buy';
    }
    elseif (isset($this->apiData['ObjectDetails']['Huur']['Huurprijs'])) {
      $this->priceType = 'rent';
    }
  }

  /**
   * {@inheritDoc}
   */
  public function parsePrice() {
    $this->parsePriceType();
    $this->price = NULL;

    if ($this->priceType == 'buy') {
      $this->price = $this->apiData['ObjectDetails']['Koop']['Koopprijs'];
    }
    elseif ($this->priceType == 'rent') {
      $this->price = $this->apiData['ObjectDetails']['Huur']['Huurprijs'];
    }
  }

  /**
   * {@inheritDoc}
   */
  public function parseCategoryTid() {
    $category = 'AGRAARISCH';
    $hoof = isset($this->apiData['AenLV']['Hoofdfunctie']) ? array_keys($this->apiData['AenLV']['Hoofdfunctie'])[0] : NULL;

    if ($hoof && ($hoof == 'ManegePensionstalling' || $hoof == 'Paardenhouderij')) {
      $category = 'HIPPISCH';
    }

    $this->categoryTid = $this->tidFromName($category, 'property_category');
  }

  /**
   * {@inheritDoc}
   */
  public function parseHouseArea() {
    $this->houseArea = NULL;
    if (isset($this->apiData['AenLV']['HuiskavelOppervlakte'])) {
      $hectare = $this->apiData['AenLV']['HuiskavelOppervlakte']['Hectare'];
      $are = $this->apiData['AenLV']['HuiskavelOppervlakte']['Are'];
      $centiare = $this->apiData['AenLV']['HuiskavelOppervlakte']['Centiare'];
      $this->houseArea = $hectare * 10000 + $are * 100 + $centiare;
    }
  }

  /**
   * {@inheritDoc}
   */
  public function parseFieldArea() {
    $this->fieldArea = NULL;
    if (isset($this->apiData['AenLV']['VeldkavelOppervlakte'])) {
      $hectare = $this->apiData['AenLV']['VeldkavelOppervlakte']['Hectare'];
      $are = $this->apiData['AenLV']['VeldkavelOppervlakte']['Are'];
      $centiare = $this->apiData['AenLV']['VeldkavelOppervlakte']['Centiare'];
      $this->fieldArea = $hectare * 10000 + $are * 100 + $centiare;
    }
  }

  /**
   * {@inheritDoc}
   */
  public function parseBusinessTypeTid() {
    if (isset($this->apiData['AenLV']['Hoofdfunctie'])) {
      $business_type = strtoupper(array_keys($this->apiData['AenLV']['Hoofdfunctie'])[0]);
    }
    else {
      $business_type = NULL;
    }
    $this->businessTypeTid = $this->tidFromName($business_type, 'business_type');
  }

  /**
   * {@inheritDoc}
   */
  public function parseSoilTypeTid() {
    $this->soilTypeTid = NULL;
    if (isset($this->apiData['AenLV']['Hoofdfunctie']['Overig']['Grondsoorten'])) {
      $soil_type = strtoupper(array_values($this->apiData['AenLV']['Hoofdfunctie']['Overig']['Grondsoorten'])[0]);
      $this->soilTypeTid = $this->tidFromName($soil_type, 'soil_type');
    }
  }

  /**
   * {@inheritDoc}
   */
  public function parseHasLivingSpace() {
    $this->hasLivingSpace = NULL;
    if (isset($this->apiData['AenLV']['Bedrijfswoningen'])) {
      $this->hasLivingSpace = $this->apiData['AenLV']['Bedrijfswoningen']['AantalBedrijfswoningen'] > 0;
    }
  }

  /**
   * {@inheritDoc}
   */
  public function parseStatusTid() {
    if (isset($this->apiData['ObjectDetails']['StatusBeschikbaarheid']['Status'])) {
      $status = str_replace(' ', '_', strtoupper($this->apiData['ObjectDetails']['StatusBeschikbaarheid']['Status']));
    }
    else {
      $status = NULL;
    }
    $this->statusTid = $this->tidFromName($status, 'property_status');
  }

  /**
   * {@inheritDoc}
   */
  public function parseLocation() {
    $geo_api_key = $this->realworksConfig->get('geocoding_api_key');

    if ($geo_api_key) {
      $address_root = $this->apiData['ObjectDetails']['Adres']['Nederlands'];
      $address_parts = [
        $address_root['Straatnaam'] . $address_root['Huisnummer']['Hoofdnummer'],
        $address_root['Postcode'] . ' ' . $address_root['Woonplaats'],
        $address_root['Provincie'],
        $address_root['Land'],
      ];
      $address = implode(', ', $address_parts);
      $params = [
        'address' => $address,
        'key' => $geo_api_key,
      ];
      $request_url = 'https://maps.googleapis.com/maps/api/geocode/json?' . http_build_query($params);

      try {
        $maps_response = $this->httpClient->get($request_url);
      }
      catch (ClientException $e) {
        $this->messenger->addWarning($e->getMessage());
        $this->location = NULL;
        return;
      }

      $maps_data = json_decode($maps_response->getBody(), TRUE);
      $location = $maps_data['results'][0]['geometry']['location'];
      $formatted_location = 'POINT (' . $location['lat'] . ' ' . $location['lng'] . ')';
    }
    else {
      $this->messenger->addError(t('Geocoding API key is empty, skipping address fields. Please set the API key in the module configuration and initiate a manual pull.'));
      $this->location = NULL;
      return;
    }

    $this->location = $formatted_location;
  }

  /**
   * {@inheritDoc}
   */
  public function parseMedia() {
    $image_fids = [];
    $document_fids = [];
    $video_fids = [];
    $map_fids = [];

    $media = isset($this->apiData['MediaLijst']['Media'][0]) ? $this->apiData['MediaLijst']['Media'] : [$this->apiData['MediaLijst']['Media']];
    foreach ($media as $media_item) {
      if (!$media_item['URL']) {
        continue;
      }
      $file_name = preg_split('/\?/', basename($media_item['URL']))[0];

      if (in_array($media_item['Groep'], ['Foto', 'HoofdFoto'])) {
        $file_type = 'image';
      }
      elseif ($media_item['Groep'] == 'Brochure') {
        $file_type = 'document';
      }
      elseif ($media_item['Groep'] == 'Overig') {
        $ext = preg_split('/\./', $file_name);
        $extension = strtolower(end($ext));
        if (in_array($extension, ['.jpg', '.jpeg', '.png', '.gif', '.webp'])) {
          $file_type = 'image';
        }
        elseif (in_array($extension, ['.webm', '.mkv', '.avi', '.mp4', '.mpeg'])) {
          $file_type = 'video';
        }
      }

      $file_path = $this->mediaDirs[$file_type] . '/' . $file_name;
      $file_id = NULL;

      $existing_file = $this->fileStorage->loadByProperties([
        'uri' => $file_path,
      ]);

      if (!$existing_file) {
        try {
          $response = $this->httpClient->get($media_item['URL']);
        }
        catch (ClientException $e) {
          $this->messenger->addWarning($e);
        }

        $file_data = $response->getBody()->getContents();
        $this->fileRepo->writeData($file_data, $file_path, FileSystemInterface::EXISTS_REPLACE);

        $file = File::create([
          'filename' => $file_name,
          'uri' => $file_path,
          'status' => 1,
          'uid' => 1,
        ]);
        $file->save();
        $file_id = $file->id();
      }
      else {
        $file = array_values($existing_file)[0];
        $file_id = $file;
      }

      switch ($file_type) {
        case 'image':
          $image_fids[] = $file_id;
          break;

        case 'document':
          $document_fids[] = $file_id;
          break;

        case 'video':
          $video_fids[] = $file_id;
          break;
      }
    }

    $this->imageFids = $image_fids;
    $this->documentFids = $document_fids;
    $this->videoFids = $video_fids;
    $this->mapFids = $map_fids;
  }

}
