<?php

namespace Drupal\realworks_api\Service;

use Drupal\Core\File\FileSystemInterface;
use Drupal\file\Entity\File;
use GuzzleHttp\Exception\ClientException;

/**
 * Class for parsing API data from a Bedrijven endpoint.
 */
class PropertyBedrijven extends PropertyBase {

  /**
   * {@inheritDoc}
   */
  public function parseSource() {
    $this->source = 'business';
  }

  /**
   * {@inheritDoc}
   */
  public function parseId() {
    if (isset($this->apiData['id'])) {
      $this->id = 'rw_bedrijven:' . $this->apiData['id'];
    }
    else {
      $this->id = NULL;
    }
  }

  /**
   * {@inheritDoc}
   */
  public function parsePriceType() {
    if (isset($this->apiData['financieel']['overdracht']['koopEnOfHuur']['koopprijs'])
      && $this->apiData['financieel']['overdracht']['koopEnOfHuur']['koopprijs'] > 0) {
      $this->priceType = 'buy';
    }
    elseif (isset($this->apiData['financieel']['overdracht']['koopEnOfHuur']['huurprijs'])
      && $this->apiData['financieel']['overdracht']['koopEnOfHuur']['huurprijs'] > 0) {
      $this->priceType = 'rent';
    }
    else {
      $this->priceType = NULL;
    }
  }

  /**
   * {@inheritDoc}
   */
  public function parsePrice() {
    if (isset($this->apiData['financieel']['overdracht']['koopEnOfHuur']['koopprijs'])
      && $this->apiData['financieel']['overdracht']['koopEnOfHuur']['koopprijs'] > 0) {
      $this->price = $this->apiData['financieel']['overdracht']['koopEnOfHuur']['koopprijs'];
    }
    elseif (isset($this->apiData['financieel']['overdracht']['koopEnOfHuur']['huurprijs'])
      && $this->apiData['financieel']['overdracht']['koopEnOfHuur']['huurprijs'] > 0) {
      $this->price = $this->apiData['financieel']['overdracht']['koopEnOfHuur']['huurprijs'];
    }
    else {
      $this->price = NULL;
    }
  }

  /**
   * {@inheritDoc}
   */
  public function parseStatusTid() {
    $status = NULL;

    if (isset($this->apiData['financieel']['overdracht']['status'])) {
      $status = $this->apiData['financieel']['overdracht']['status'];
    }
    elseif (isset($this->apiData['status'])) {
      $status = $this->apiData['status'];
    }

    $this->statusTid = $this->tidFromName($status, 'property_status');
  }

  /**
   * {@inheritDoc}
   */
  public function parsePriceRentType() {
    $price_rent_type = NULL;

    if (!isset($this->apiData['financieel']['overdracht']['koopEnOfHuur']['huurconditie'])) {
      $this->priceRentType = NULL;
      return;
    }

    switch ($this->apiData['financieel']['overdracht']['koopEnOfHuur']['huurconditie']) {
      case 'PER_MAAND':
        $price_rent_type = 'month';
        break;

      case 'PER_JAAR':
        $price_rent_type = 'year';
        break;

      case 'PER_VIERKANTE_METERS_PER_JAAR':
        $price_rent_type = 'year_m2';
        break;
    }

    $this->priceRentType = $price_rent_type;
  }

  /**
   * {@inheritDoc}
   */
  public function parsePriceRent() {
    if (isset($this->apiData['financieel']['overdracht']['koopEnOfHuur']['huurprijs'])) {
      $this->priceRent = $this->apiData['financieel']['overdracht']['koopEnOfHuur']['huurprijs'];
    }
    else {
      $this->priceRent = NULL;
    }
  }

  /**
   * {@inheritDoc}
   */
  public function parseCategoryTid() {
    if (isset($this->apiData['kenmerken']['hoofdfunctie'])) {
      $category = $this->apiData['kenmerken']['hoofdfunctie'];
    }
    else {
      $category = NULL;
    }
    $this->categoryTid = $this->tidFromName($category, 'property_category');
  }

  /**
   * {@inheritDoc}
   */
  public function parseBuildYear() {
    if (isset($this->apiData['gebouwdetails']['bouwjaar']['bouwjaar1'])) {
      $this->buildYear = $this->apiData['gebouwdetails']['bouwjaar']['bouwjaar1'];
    }
    else {
      $this->buildYear = NULL;
    }
  }

  /**
   * {@inheritDoc}
   */
  public function parseEnergyLabelTid() {
    if (isset($this->apiData['gebouwdetails']['energielabel']['energieklasse'])) {
      $energy_label = $this->apiData['gebouwdetails']['energielabel']['energieklasse'];
    }
    else {
      $energy_label = NULL;
    }

    $this->energyLabelTid = $this->tidFromName($energy_label, 'energy_label');
  }

  /**
   * {@inheritDoc}
   */
  public function parseParkingCapacity() {
    if (isset($this->apiData['gebouwdetails']['lokatie']['parkeren']['overdekteParkeerplaatsenAantal'])
      && isset($this->apiData['gebouwdetails']['lokatie']['parkeren']['nietOverdekteParkeerplaatsenAantal'])) {
      $this->parkingCapacity = intval($this->apiData['gebouwdetails']['lokatie']['parkeren']['overdekteParkeerplaatsenAantal'])
        + intval($this->apiData['gebouwdetails']['lokatie']['parkeren']['nietOverdekteParkeerplaatsenAantal']);
    }
    else {
      $this->parkingCapacity = NULL;
    }
  }

  /**
   * {@inheritDoc}
   */
  public function parseDescription() {
    if (isset($this->apiData['teksten']['aanbiedingstekst'])) {
      $this->description = $this->apiData['teksten']['aanbiedingstekst'];
    }
    else {
      $this->description = NULL;
    }
  }

  /**
   * {@inheritDoc}
   */
  public function parseLocation() {
    $geo_api_key = $this->realworksConfig->get('geocoding_api_key');

    if ($geo_api_key) {
      $address_parts = [
        $this->apiData['straat'] . $this->apiData['huisnummer'],
        $this->apiData['postcode'] . ' ' . $this->apiData['plaats'],
        $this->apiData['provincie'],
        $this->apiData['land'],
      ];
      $address = implode(', ', $address_parts);
      $params = [
        'address' => $address,
        'key' => $geo_api_key,
      ];
      $request_url = 'https://maps.googleapis.com/maps/api/geocode/json?' . http_build_query($params);

      try {
        $maps_response = $this->httpClient->get($request_url);
      }
      catch (ClientException $e) {
        $this->messenger->addWarning($e->getMessage());
        $this->location = NULL;
        return;
      }

      $maps_data = json_decode($maps_response->getBody(), TRUE);
      $location = $maps_data['results'][0]['geometry']['location'];
      $formatted_location = 'POINT (' . $location['lat'] . ' ' . $location['lng'] . ')';
    }
    else {
      $this->messenger->addError(t('Geocoding API key is empty, skipping address fields. Please set the API key in the module configuration and initiate a manual pull.'));
      $this->location = NULL;
      return;
    }

    $this->location = $formatted_location;
  }

  /**
   * {@inheritDoc}
   */
  public function parseMedia() {
    $image_fids = [];
    $document_fids = [];
    $video_fids = [];
    $map_fids = [];

    foreach ($this->apiData['media'] as $media_item) {
      switch ($media_item['soort']) {
        case 'HOOFDFOTO':
        case 'FOTO':
          $file_type = 'image';
          break;

        case 'DOCUMENT':
          $file_type = 'document';
          break;

        case 'VIDEO':
          $file_type = 'video';
          break;

        case 'PLATTEGROND':
          $file_type = 'map';
          break;
      }

      if (!isset($file_type)) {
        continue;
      }

      $file_name = preg_split('/\?/', basename($media_item['link']))[0];
      $file_path = $this->mediaDirs[$file_type] . '/' . $file_name;
      $file_id = NULL;

      $existing_file = $this->fileStorage->loadByProperties([
        'uri' => $file_path,
      ]);

      if (!$existing_file) {
        try {
          $response = $this->httpClient->get($media_item['link']);
        }
        catch (ClientException $e) {
          $this->messenger->addWarning($e->getMessage());
        }

        $file_data = $response->getBody()->getContents();
        $this->fileRepo->writeData($file_data, $file_path, FileSystemInterface::EXISTS_REPLACE);

        $file = File::create([
          'filename' => $file_name,
          'uri' => $file_path,
          'status' => 1,
          'uid' => 1,
        ]);
        if ($media_item['mimetype']) {
          $file->setMimeType($media_item['mimetype']);
        }

        $file->save();
        $file_id = $file->id();
      }
      else {
        $file = array_values($existing_file)[0];
        $file_id = $file;
      }

      switch ($media_item['soort']) {
        case 'HOOFDFOTO':
        case 'FOTO':
          $image_fids[] = $file_id;
          break;

        case 'DOCUMENT':
          $document_fids[] = $file_id;
          break;

        case 'VIDEO':
          $video_fids[] = $file_id;
          break;

        case 'PLATTEGROND':
          $map_fids[] = $file_id;
          break;
      }
    }

    $this->imageFids = $image_fids;
    $this->documentFids = $document_fids;
    $this->videoFids = $video_fids;
    $this->mapFids = $map_fids;
  }

}
