<?php

namespace Drupal\realworks_api\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;

/**
 * The Realworks API sync service class.
 */
class RealworksSync {
  /**
   * The realworks configuration object.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $realworksConfig;
  /**
   * The Guzzle HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;
  /**
   * The Drupal entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;
  /**
   * The Wonen type endpoint processing service.
   *
   * @var \Drupal\realworks_api\Service\PropertyWonen
   */
  protected $wonen;
  /**
   * The Bedrijven type endpoint processing service.
   *
   * @var \Drupal\realworks_api\Service\PropertyBedrijven
   */
  protected $bedrijven;
  /**
   * The Agrarisch type endpoint processing service.
   *
   * @var \Drupal\realworks_api\Service\PropertyAgrarisch
   */
  protected $agrarisch;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   The Drupal configuration object.
   * @param \GuzzleHttp\ClientInterface $httpClient
   *   The Guzzle HTTP client.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The Drupal entity type manager.
   * @param \Drupal\realworks_api\Service\PropertyWonen $wonen
   *   The Wonen type endpoint processing service.
   * @param \Drupal\realworks_api\Service\PropertyBedrijven $bedrijven
   *   The Bedrijven type endpoint processing service.
   * @param \Drupal\realworks_api\Service\PropertyAgrarisch $agrarisch
   *   The Agrarisch type endpoint processing service.
   */
  public function __construct(ConfigFactoryInterface $config,
                              ClientInterface $httpClient,
                              EntityTypeManagerInterface $entityTypeManager,
                              PropertyWonen $wonen,
                              PropertyBedrijven $bedrijven,
                              PropertyAgrarisch $agrarisch) {
    $realworksConfig = $config->get('realworks_api.settings');
    $this->realworksConfig = $realworksConfig;
    $this->httpClient = $httpClient;
    $this->entityTypeManager = $entityTypeManager;
    $this->wonen = $wonen;
    $this->bedrijven = $bedrijven;
    $this->agrarisch = $agrarisch;
  }

  /**
   * Fetches all the items available from the API endpoints.
   */
  public function getItems() {
    $items = [];

    $wonen_endpoint = $this::parseConfig($this->realworksConfig->get('wonen'));
    $bedrijven_endpoint = $this::parseConfig($this->realworksConfig->get('bedrijven'));
    $agrarisch_endpoint = $this::parseConfig($this->realworksConfig->get('agrarisch'));

    foreach ($wonen_endpoint as $endpoint) {
      try {
        $response = $this->httpClient->get($endpoint['url'], [
          'headers' => [
            'Authorization' => 'rwauth ' . $endpoint['key'],
            'Accept' => 'application/json; charset=UTF-8',
          ],
        ]);
      }
      catch (ClientException $e) {
        continue;
      }

      $data = json_decode($response->getBody(), TRUE);

      foreach ($data['resultaten'] as $result) {
        $items[] = ['type' => 'wonen', 'data' => $result];
      }
    }

    foreach ($bedrijven_endpoint as $endpoint) {
      try {
        $response = $this->httpClient->get($endpoint['url'], [
          'headers' => [
            'Authorization' => 'rwauth ' . $endpoint['key'],
            'Accept' => 'application/json; charset=UTF-8',
          ],
        ]);
      }
      catch (ClientException $e) {
        continue;
      }

      $data = json_decode($response->getBody(), TRUE);

      foreach ($data['resultaten'] as $result) {
        $items[] = ['type' => 'bedrijven', 'data' => $result];
      }
    }

    foreach ($agrarisch_endpoint as $endpoint) {
      try {
        $response = $this->httpClient->get($endpoint['url']);
      }
      catch (ClientException $e) {
        continue;
      }

      $body = $response->getBody();

      // @todo Do this unziping better
      $file = fopen('/tmp/rw_archive', 'wb');
      fwrite($file, $body->getContents());
      fclose($file);
      $zip = new \ZipArchive();
      $zip->open('/tmp/rw_archive');
      $filename = $zip->getNameIndex(0);
      if (!str_ends_with($filename, '.xml')) {
        $filename = $zip->getNameIndex(1);
      }
      $zip->extractTo('/tmp/rw_extracted');
      $content = file_get_contents('/tmp/rw_extracted/' . $filename);
      $zip->close();

      $xml_object = simplexml_load_string($content);
      $json_data = json_encode($xml_object);
      $data = json_decode($json_data, TRUE);
      $objects = isset($data['Object'][0]) ? $data['Object'] : [$data['Object']];

      foreach ($objects as $result) {
        $items[] = ['type' => 'agrarisch', 'data' => $result];
      }
    }

    return $items;
  }

  /**
   * Processes a property using data pulled from the API and creates a node.
   */
  public function processItem($item) {
    if ($item['type'] == 'wonen') {
      $existing = $this->entityTypeManager->getStorage('node')->loadByProperties([
        'type' => 'property',
        'field_api_id' => 'rw_wonen:' . $item['data']['id'],
      ]);

      if ($existing && !(array_values($existing)[0]->field_api_inactive->value)) {
        return;
      }

      $this->wonen->setData($item['data'], $existing);
      $this->wonen->parseAll();
      $this->wonen->createNode();
    }
    elseif ($item['type'] == 'bedrijven') {
      $existing = $this->entityTypeManager->getStorage('node')->loadByProperties([
        'type' => 'property',
        'field_api_id' => 'rw_bedrijven:' . $item['data']['id'],
      ]);

      if ($existing && !(array_values($existing)[0]->field_api_inactive->value)) {
        return;
      }

      $this->bedrijven->setData($item['data'], $existing);
      $this->bedrijven->parseAll();
      $this->bedrijven->createNode();
    }
    elseif ($item['type'] == 'agrarisch') {
      $existing = $this->entityTypeManager->getStorage('node')->loadByProperties([
        'type' => 'property',
        'field_api_id' => 'rw_agrarisch:' . $item['data']['ObjectSystemID'],
      ]);

      if ($existing && !(array_values($existing)[0]->field_api_inactive->value)) {
        return;
      }

      $this->agrarisch->setData($item['data'], $existing);
      $this->agrarisch->parseAll();
      $this->agrarisch->createNode();
    }
  }

  /**
   * Parses the configuration provided in the Drupal admin interface.
   */
  private static function parseConfig($config) {
    if (!$config) {
      return [];
    }

    $lines = preg_split("/\r\n|\n|\r/", $config);
    $info = [];
    foreach ($lines as $line) {
      if (strlen($line) < 10) {
        continue;
      }

      $parts = explode('|', $line);

      $info[] = [
        'url' => trim($parts[0]),
        'key' => isset($parts[1]) ? trim($parts[1]) : '',
      ];
    }

    return $info;
  }

}
