<?php

namespace Drupal\realworks_api\Service;

use Drupal\Core\File\FileSystemInterface;
use Drupal\file\Entity\File;
use GuzzleHttp\Exception\ClientException;

/**
 * Class for parsing API data from a Wonen endpoint.
 */
class PropertyWonen extends PropertyBase {

  /**
   * {@inheritDoc}
   */
  public function parseSource() {
    $this->source = 'house';
  }

  /**
   * {@inheritDoc}
   */
  public function parseId() {
    if (isset($this->apiData['id'])) {
      $this->id = 'rw_wonen:' . $this->apiData['id'];
    }
    else {
      $this->id = NULL;
    }
  }

  /**
   * {@inheritDoc}
   */
  public function parsePriceType() {
    if (isset($this->apiData['financieel']['overdracht']['koopprijs']) && $this->apiData['financieel']['overdracht']['koopprijs'] > 0) {
      $this->priceType = 'buy';
    }
    elseif (isset($this->apiData['financieel']['overdracht']['huurprijs']) && $this->apiData['financieel']['overdracht']['huurprijs'] > 0) {
      $this->priceType = 'rent';
    }
    else {
      $this->priceType = NULL;
    }
  }

  /**
   * {@inheritDoc}
   */
  public function parsePrice() {
    if (isset($this->apiData['financieel']['overdracht']['koopprijs']) && $this->apiData['financieel']['overdracht']['koopprijs'] > 0) {
      $this->price = $this->apiData['financieel']['overdracht']['koopprijs'];
    }
    elseif (isset($this->apiData['financieel']['overdracht']['huurprijs']) && $this->apiData['financieel']['overdracht']['huurprijs'] > 0) {
      $this->price = $this->apiData['financieel']['overdracht']['huurprijs'];
    }
    else {
      $this->price = NULL;
    }
  }

  /**
   * {@inheritDoc}
   */
  public function parseStatusTid() {
    if (isset($this->apiData['financieel']['overdracht']['status'])) {
      $status = $this->apiData['financieel']['overdracht']['status'];
    }
    else {
      $status = NULL;
    }

    $this->statusTid = $this->tidFromName($status, 'property_status');
  }

  /**
   * {@inheritDoc}
   */
  public function parseCategoryTid() {
    $category = NULL;

    if ((isset($this->apiData['object']['type']['objecttype']) && $this->apiData['object']['type']['objecttype'] == 'BOUWGROND')
        || (isset($this->apiData['algemeen']['bouwvorm']) && $this->apiData['algemeen']['bouwvorm'] == 'NIEUWBOUW')) {
      $category = 'NIEUWBOUW';
    }
    elseif (isset($this->apiData['marketing']['website']['prioriteit']) && $this->apiData['marketing']['website']['prioriteit'] == 80) {
      $category = 'BUITENSTATE';
    }
    elseif (isset($this->apiData['algemeen']['bijzonderheden']) && in_array('MONUMENTAAL', $this->apiData['algemeen']['bijzonderheden'])) {
      $category = 'MONUMENTAAL';
    }
    else {
      $category = NULL;
    }

    $this->categoryTid = $this->tidFromName($category, 'property_category');
  }

  /**
   * {@inheritDoc}
   */
  public function parseLivableArea() {
    if (isset($this->apiData['algemeen']['woonoppervlakte'])) {
      $this->livableArea = $this->apiData['algemeen']['woonoppervlakte'];
    }
    else {
      $this->livableArea = NULL;
    }
  }

  /**
   * {@inheritDoc}
   */
  public function parseRoomCount() {
    if (isset($this->apiData['algemeen']['aantalKamers'])) {
      $this->roomCount = $this->apiData['algemeen']['aantalKamers'];
    }
    else {
      $this->roomCount = NULL;
    }
  }

  /**
   * {@inheritDoc}
   */
  public function parseBedroomCount() {
    if (!isset($this->apiData['detail']['etages'])) {
      $this->bedroomCount = NULL;
      return;
    }

    $bedroom_count = 0;

    foreach ($this->apiData['detail']['etages'] as $etage) {
      $bedroom_count += $etage['aantalKamers'];
    }

    $this->bedroomCount = $bedroom_count;
  }

  /**
   * {@inheritDoc}
   */
  public function parseBuildYear() {
    if (isset($this->apiData['algemeen']['bouwjaar'])) {
      $this->buildYear = $this->apiData['algemeen']['bouwjaar'];
    }
    else {
      $this->buildYear = NULL;
    }
  }

  /**
   * {@inheritDoc}
   */
  public function parseEnergyLabelTid() {
    if (isset($this->apiData['algemeen']['energieklasse'])) {
      $energy_label = $this->apiData['algemeen']['energieklasse'];
    }
    else {
      $energy_label = NULL;
    }

    $this->energyLabelTid = $this->tidFromName($energy_label, 'energy_label');
  }

  /**
   * {@inheritDoc}
   */
  public function parseSituationTids() {
    if (!isset($this->apiData['algemeen']['liggingen'])) {
      $this->situationTids = [];
      return;
    }

    $situations = $this->apiData['algemeen']['liggingen'];
    $situation_tids = [];

    foreach ($situations as $situation) {
      $situation_tids[] = $this->tidFromName($situation, 'property_situation');
    }

    $this->situationTids = $situation_tids;
  }

  /**
   * {@inheritDoc}
   */
  public function parseGarageTypeTids() {
    if (!isset($this->apiData['detail']['buitenruimte']['garagesoorten'])) {
      $this->garageTypeTids = [];
      return;
    }
    $garage_types = $this->apiData['detail']['buitenruimte']['garagesoorten'];
    $garage_type_tids = [];

    foreach ($garage_types as $garage_type) {
      $garage_type_tids[] = $this->tidFromName($garage_type, 'garage_type');
    }

    $this->garageTypeTids = $garage_type_tids;
  }

  /**
   * {@inheritDoc}
   */
  public function parseDescription() {
    if (isset($this->apiData['teksten']['aanbiedingstekst'])) {
      $this->description = $this->apiData['teksten']['aanbiedingstekst'];
    }
    else {
      $this->description = NULL;
    }
  }

  /**
   * {@inheritDoc}
   */
  public function parseLocation() {
    $geo_api_key = $this->realworksConfig->get('geocoding_api_key');

    if ($geo_api_key) {
      $address_parts = [
        $this->apiData['adres']['straat'] . ' ' . $this->apiData['adres']['straat2']
        . $this->apiData['adres']['huisnummer'] . ' ' . $this->apiData['adres']['huisnummertoevoeging'],
        $this->apiData['adres']['postcode'] . ' ' .
        $this->apiData['adres']['plaats'],
        $this->apiData['adres']['provincie'],
        $this->apiData['adres']['land'],
      ];
      $address = implode(', ', $address_parts);
      $params = [
        'address' => $address,
        'key' => $geo_api_key,
      ];
      $request_url = 'https://maps.googleapis.com/maps/api/geocode/json?' . http_build_query($params);

      try {
        $maps_response = $this->httpClient->get($request_url);
      }
      catch (ClientException $e) {
        $this->messenger->addWarning($e->getMessage());
        $this->location = NULL;
        return;
      }

      $maps_data = json_decode($maps_response->getBody(), TRUE);
      $location = $maps_data['results'][0]['geometry']['location'];
      $formatted_location = 'POINT (' . $location['lat'] . ' ' . $location['lng'] . ')';
    }
    else {
      $this->messenger->addError(t('Geocoding API key is empty, skipping address fields. Please set the API key in the module configuration and initiate a manual pull.'));
      $this->location = NULL;
      return;
    }

    $this->location = $formatted_location;
  }

  /**
   * {@inheritDoc}
   */
  public function parseMedia() {
    $image_fids = [];
    $document_fids = [];
    $video_fids = [];
    $map_fids = [];

    foreach ($this->apiData['media'] as $media_item) {
      switch ($media_item['soort']) {
        case 'HOOFDFOTO':
        case 'FOTO':
          $file_type = 'image';
          break;

        case 'DOCUMENT':
          $file_type = 'document';
          break;

        case 'VIDEO':
          $file_type = 'video';
          break;

        case 'PLATTEGROND':
          $file_type = 'map';
          break;
      }

      if (!isset($file_type)) {
        continue;
      }

      $file_name = preg_split('/\?/', basename($media_item['link']))[0];
      $file_path = $this->mediaDirs[$file_type] . '/' . $file_name;
      $file_id = NULL;

      $existing_file = $this->fileStorage->loadByProperties([
        'uri' => $file_path,
      ]);

      if (!$existing_file) {
        try {
          $response = $this->httpClient->get($media_item['link']);
        }
        catch (ClientException $e) {
          $this->messenger->addWarning($e->getMessage());
          continue;
        }

        $file_data = $response->getBody()->getContents();
        $this->fileRepo->writeData($file_data, $file_path, FileSystemInterface::EXISTS_REPLACE);

        $file = File::create([
          'filename' => $file_name,
          'uri' => $file_path,
          'status' => 1,
          'uid' => 1,
        ]);
        if ($media_item['mimetype']) {
          $file->setMimeType($media_item['mimetype']);
        }

        $file->save();
        $file_id = $file->id();
      }
      else {
        $file = array_values($existing_file)[0];
        $file_id = $file;
      }

      switch ($media_item['soort']) {
        case 'HOOFDFOTO':
        case 'FOTO':
          $image_fids[] = $file_id;
          break;

        case 'DOCUMENT':
          $document_fids[] = $file_id;
          break;

        case 'VIDEO':
          $video_fids[] = $file_id;
          break;

        case 'PLATTEGROND':
          $map_fids[] = $file_id;
          break;
      }
    }

    $this->imageFids = $image_fids;
    $this->documentFids = $document_fids;
    $this->videoFids = $video_fids;
    $this->mapFids = $map_fids;
  }

}
