<?php

namespace Drupal\realworks_api\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\file\FileRepositoryInterface;
use Drupal\node\Entity\Node;
use Drupal\taxonomy\Entity\Term;
use GuzzleHttp\ClientInterface;

/**
 * Base class for parsing a property and creating a node.
 */
class PropertyBase implements PropertyInterface {
  /**
   * The directories for media files.
   *
   * @var array
   */
  protected $mediaDirs = [
    'image' => 'public://images',
    'document' => 'public://documents',
    'video' => 'public://videos',
    'map' => 'public://maps',
  ];
  /**
   * The Drupal file system interface.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;
  /**
   * The Realworks API moule configuration.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $realworksConfig;
  /**
   * The Guzzle HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;
  /**
   * The Drupal taxonomy term storage.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $termStorage;
  /**
   * The Drupal file storage.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $fileStorage;
  /**
   * The Drupal messenger.
   *
   * @var \Drupal\Core\Messenger\Messenger
   */
  protected $messenger;
  /**
   * The Drupal file repository.
   *
   * @var \Drupal\file\FileRepositoryInterface
   */
  protected $fileRepo;

  /**
   * The pulled API data.
   *
   * @var array
   */
  public $apiData = NULL;
  /**
   * The existing nodes with the same id.
   *
   * @var array
   */
  public $existing = NULL;
  /**
   * The price type.
   *
   * @var string
   */
  public $priceType = NULL;
  /**
   * The price.
   *
   * @var float
   */
  public $price = NULL;
  /**
   * The API source.
   *
   * @var string
   */
  public $source = NULL;
  /**
   * The status TID.
   *
   * @var int
   */
  public $statusTid = NULL;
  /**
   * The rent price.
   *
   * @var float
   */
  public $priceRent = NULL;
  /**
   * The rent price type.
   *
   * @var string
   */
  public $priceRentType = NULL;
  /**
   * The category TID.
   *
   * @var int
   */
  public $categoryTid = NULL;
  /**
   * The livable area.
   *
   * @var float
   */
  public $livableArea = NULL;
  /**
   * The room count.
   *
   * @var int
   */
  public $roomCount = NULL;
  /**
   * The bedroom count.
   *
   * @var int
   */
  public $bedroomCount = NULL;
  /**
   * The build year.
   *
   * @var int
   */
  public $buildYear = NULL;
  /**
   * The energy label TID.
   *
   * @var int
   */
  public $energyLabelTid = NULL;
  /**
   * The parking capacity.
   *
   * @var int
   */
  public $parkingCapacity = NULL;
  /**
   * The situation TIDs.
   *
   * @var array
   */
  public $situationTids = NULL;
  /**
   * The garage type TIDs.
   *
   * @var array
   */
  public $garageTypeTids = NULL;
  /**
   * The house area.
   *
   * @var float
   */
  public $houseArea = NULL;
  /**
   * The field area.
   *
   * @var float
   */
  public $fieldArea = NULL;
  /**
   * The business type TID.
   *
   * @var int
   */
  public $businessTypeTid = NULL;
  /**
   * The soil type TID.
   *
   * @var int
   */
  public $soilTypeTid = NULL;
  /**
   * If the property has living space or not.
   *
   * @var bool
   */
  public $hasLivingSpace = NULL;
  /**
   * Image file IDs.
   *
   * @var array
   */
  public $imageFids = NULL;
  /**
   * Document file IDs.
   *
   * @var array
   */
  public $documentFids = NULL;
  /**
   * Video file IDs.
   *
   * @var array
   */
  public $videoFids = NULL;
  /**
   * Map file IDs.
   *
   * @var array
   */
  public $mapFids = NULL;
  /**
   * The description.
   *
   * @var string
   */
  public $description = NULL;
  /**
   * The location.
   *
   * @var string
   */
  public $location = NULL;
  /**
   * The ID.
   *
   * @var string
   */
  public $id = NULL;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The Drupal file system interface.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   The Drupal config interface.
   * @param \GuzzleHttp\ClientInterface $httpClient
   *   The Guzzle HTTP client.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The Drupal entity type manager.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   The Drupal messenger.
   * @param \Drupal\file\FileRepositoryInterface $fileRepo
   *   The Drupal file repository.
   */
  public function __construct(FileSystemInterface $fileSystem,
                                ConfigFactoryInterface $config,
                                ClientInterface $httpClient,
                                EntityTypeManagerInterface $entityTypeManager,
                                Messenger $messenger,
                                FileRepositoryInterface $fileRepo) {
    $this->fileSystem = $fileSystem;
    $realworksConfig = $config->get('realworks_api.settings');
    $this->realworksConfig = $realworksConfig;
    $this->httpClient = $httpClient;
    $this->termStorage = $entityTypeManager->getStorage('taxonomy_term');
    $this->fileStorage = $entityTypeManager->getStorage('file');
    $this->messenger = $messenger;
    $this->fileRepo = $fileRepo;
  }

  /**
   * {@inheritDoc}
   */
  public function setData($apiData, $existing) {
    $this->apiData = $apiData;
    $this->existing = $existing;
  }

  /**
   * {@inheritDoc}
   */
  public function createNode() {
    if (!$this->existing) {
      $new_node = Node::create([
        'type' => 'property',
        'title' => $this->id,
        'field_priceType' => $this->priceType,
        'field_property_source' => $this->source,
        'field_property_status' => $this->statusTid,
        'field_price' => $this->price,
        'field_priceRent' => $this->priceRent,
        'field_priceRentType' => $this->priceRentType,
        'field_property_category' => $this->categoryTid,
        'field_livableArea' => $this->livableArea,
        'field_roomCount' => $this->roomCount,
        'field_bedroomCount' => $this->bedroomCount,
        'field_buildYear' => $this->buildYear,
        'field_energy_label' => $this->energyLabelTid,
        'field_parkingCapacity' => $this->parkingCapacity,
        'field_situation' => $this->situationTids,
        'field_garage' => $this->garageTypeTids,
        'field_houseArea' => $this->houseArea,
        'field_fieldArea' => $this->fieldArea,
        'field_business_type' => $this->businessTypeTid,
        'field_soil_type' => $this->soilTypeTid,
        'field_hasLivingSpace' => $this->hasLivingSpace,
        'field_images' => $this->imageFids,
        'field_documents' => $this->documentFids,
        'field_videos' => $this->videoFids,
        'field_maps' => $this->mapFids,
        'field_description' => $this->description,
        'field_location' => $this->location,
        'field_api_id' => $this->id,
        'field_apiData' => json_encode($this->apiData),
        'field_api_inactive' => TRUE,
      ]);
      $new_node->save();
    }
    else {
      $node = Node::load(array_keys($this->existing)[0]);
      $node->title = $this->id;
      $node->field_priceType = $this->priceType;
      $node->field_property_source = $this->source;
      $node->field_property_status = $this->statusTid;
      $node->field_price = $this->price;
      $node->field_priceRent = $this->priceRent;
      $node->field_priceRentType = $this->priceRentType;
      $node->field_property_category = $this->categoryTid;
      $node->field_livableArea = $this->livableArea;
      $node->field_roomCount = $this->roomCount;
      $node->field_bedroomCount = $this->bedroomCount;
      $node->field_buildYear = $this->buildYear;
      $node->field_energy_label = $this->energyLabelTid;
      $node->field_parkingCapacity = $this->parkingCapacity;
      $node->field_situation = $this->situationTids;
      $node->field_garage = $this->garageTypeTids;
      $node->field_houseArea = $this->houseArea;
      $node->field_fieldArea = $this->fieldArea;
      $node->field_business_type = $this->businessTypeTid;
      $node->field_soil_type = $this->soilTypeTid;
      $node->field_hasLivingSpace = $this->hasLivingSpace;
      $node->field_images = $this->imageFids;
      $node->field_documents = $this->documentFids;
      $node->field_videos = $this->videoFids;
      $node->field_maps = $this->mapFids;
      $node->field_description = $this->description;
      $node->field_location = $this->location;
      $node->field_api_id = $this->id;
      $node->field_apiData = json_encode($this->apiData);
      $node->field_api_inactive = TRUE;
      $node->save();
    }
  }

  /**
   * {@inheritDoc}
   */
  public function parseAll() {
    $this->parsePriceType();
    $this->parsePrice();
    $this->parseSource();
    $this->parseStatusTid();
    $this->parsePriceRent();
    $this->parsePriceRentType();
    $this->parseCategoryTid();
    $this->parseLivableArea();
    $this->parseRoomCount();
    $this->parseBedroomCount();
    $this->parseBuildYear();
    $this->parseEnergyLabelTid();
    $this->parseParkingCapacity();
    $this->parseSituationTids();
    $this->parseGarageTypeTids();
    $this->parseHouseArea();
    $this->parseFieldArea();
    $this->parseBusinessTypeTid();
    $this->parseSoilTypeTid();
    $this->parseHasLivingSpace();
    $this->parseMedia();
    $this->parseDescription();
    $this->parseLocation();
    $this->parseId();
  }

  /**
   * Parses price type from pulled API data.
   */
  public function parsePriceType() {}

  /**
   * Parses price from pulled API data.
   */
  public function parsePrice() {}

  /**
   * Parses source from pulled API data.
   */
  public function parseSource() {}

  /**
   * Parses status from pulled API data.
   */
  public function parseStatusTid() {}

  /**
   * Parses rent price from pulled API data.
   */
  public function parsePriceRent() {}

  /**
   * Parses rent price type from pulled API data.
   */
  public function parsePriceRentType() {}

  /**
   * Parses category from pulled API data.
   */
  public function parseCategoryTid() {}

  /**
   * Parses livable area from pulled API data.
   */
  public function parseLivableArea() {}

  /**
   * Parses room count from pulled API data.
   */
  public function parseRoomCount() {}

  /**
   * Parses bedroom count from pulled API data.
   */
  public function parseBedroomCount() {}

  /**
   * Parses build year from pulled API data.
   */
  public function parseBuildYear() {}

  /**
   * Parses energy label from pulled API data.
   */
  public function parseEnergyLabelTid() {}

  /**
   * Parses parking capacity from pulled API data.
   */
  public function parseParkingCapacity() {}

  /**
   * Parses situation from pulled API data.
   */
  public function parseSituationTids() {}

  /**
   * Parses garage type from pulled API data.
   */
  public function parseGarageTypeTids() {}

  /**
   * Parses house area from pulled API data.
   */
  public function parseHouseArea() {}

  /**
   * Parses field area from pulled API data.
   */
  public function parseFieldArea() {}

  /**
   * Parses business type from pulled API data.
   */
  public function parseBusinessTypeTid() {}

  /**
   * Parses soil type from pulled API data.
   */
  public function parseSoilTypeTid() {}

  /**
   * Parses has living space from pulled API data.
   */
  public function parseHasLivingSpace() {}

  /**
   * Parses media from pulled API data.
   */
  public function parseMedia() {}

  /**
   * Parses description from pulled API data.
   */
  public function parseDescription() {}

  /**
   * Parses location from pulled API data.
   */
  public function parseLocation() {}

  /**
   * Parses id from pulled API data.
   */
  public function parseId() {}

  /**
   * {@inheritDoc}
   */
  public function prepareMediaDirectories() {
    foreach ($this->mediaDirs as $dir) {
      $this->fileSystem->prepareDirectory($dir, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);
    }
  }

  /**
   * Gets Term ID from a given Term name and Vocabulary ID.
   */
  protected function tidFromName($name, $vid) {
    if ($name == NULL || $vid == NULL) {
      return NULL;
    }

    $exists = $this->termStorage->loadByProperties(['name' => $name]);

    if (count($exists) == 0) {
      $new_term = Term::create([
        'name' => $name,
        'vid' => $vid,
      ]);
      $new_term->save();
      return $new_term->id();
    }

    return array_keys($exists)[0];
  }

}
